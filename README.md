# docker-vdr-server

Using of VDR in a docker container


# Getting started



## Prerequisites

* a working docker environment
* docker-compose

## Prepare the vdr-server image
* change to ./vdr
* check the dockerfile
* start build-dockerimage.sh

## start the container
* change to ./
* start the container for a first test with "docker-compose up"
* change the config in ./cfg to your needs
