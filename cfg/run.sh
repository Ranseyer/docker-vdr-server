#!/bin/bash


# Deutsch
export LANG=de_DE.UTF-8
export LANGUAGE=
export LC_CTYPE="de_DE.UTF-8"
export LC_NUMERIC="de_DE.UTF-8"
export LC_TIME="de_DE.UTF-8"
export LC_COLLATE="de_DE.UTF-8"
export LC_MONETARY="de_DE.UTF-8"
export LC_MESSAGES="de_DE.UTF-8"
export LC_PAPER="de_DE.UTF-8"
export LC_NAME="de_DE.UTF-8"
export LC_ADDRESS="de_DE.UTF-8"
export LC_TELEPHONE="de_DE.UTF-8"
export LC_MEASUREMENT="de_DE.UTF-8"
export LC_IDENTIFICATION="de_DE.UTF-8"
export LC_ALL=






# Set the uid:gid to run as
[ "$vdr_uid" ] && usermod  -o -u "$vdr_uid" vdr
[ "$vdr_gid" ] && groupmod -o -g "$vdr_gid" vdr

# Set folder permissions
# chown -r /recordings only if owned by root. We asume that means it's a docker volume
[ "$(stat -c %u:%g /recordings)" = "0:0" ] && chown vdr:vdr /recordings
[ "$(stat -c %u:%g /var/cache/vdr)" = "0:0" ] && chown vdr:vdr /var/cache/vdr
[ "$(stat -c %u:%g /var/lib/vdr)" = "0:0" ] && chown vdr:vdr /var/lib/vdr

vdr -v /recordings -c /etc/vdr -s -E /var/cache/vdr/epg.data -u root \
    --port 6419 --dirnames=,,1 -w 60 -P epgsearchonly -P streamdev-server -P vnsiserver -P epgsearch -P live
